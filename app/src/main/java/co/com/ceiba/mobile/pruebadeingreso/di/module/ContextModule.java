package co.com.ceiba.mobile.pruebadeingreso.di.module;

import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
@Module
public abstract class ContextModule {

    @Binds
    abstract Context provideContext(Application application);
}
