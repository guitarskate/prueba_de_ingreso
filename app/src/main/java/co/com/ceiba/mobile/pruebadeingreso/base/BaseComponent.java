package co.com.ceiba.mobile.pruebadeingreso.base;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.LayoutRes;
import androidx.constraintlayout.widget.ConstraintLayout;

import butterknife.ButterKnife;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
public abstract class BaseComponent extends ConstraintLayout {

    public BaseComponent(Context context) {
        super(context);
        initComponent();
    }

    public BaseComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        initComponent();
    }

    public BaseComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initComponent();
    }

    private void initComponent() {
        View view =  LayoutInflater.from(getContext()).inflate(layoutRes(), this, true);
        ButterKnife.bind(this, view);
    }

    @LayoutRes
    protected abstract int layoutRes();

}