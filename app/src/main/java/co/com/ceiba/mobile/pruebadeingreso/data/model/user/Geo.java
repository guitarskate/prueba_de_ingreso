package co.com.ceiba.mobile.pruebadeingreso.data.model.user;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import co.com.ceiba.mobile.pruebadeingreso.di.util.Converters;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
@Entity(tableName = "geo")
@TypeConverters(Converters.class)
public class Geo {

    @ColumnInfo(name = "lat")
    @SerializedName("lat")
    private String lat;

    @ColumnInfo(name = "lng")
    @SerializedName("lng")
    private String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

}