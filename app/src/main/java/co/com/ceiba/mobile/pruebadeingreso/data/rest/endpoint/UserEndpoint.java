package co.com.ceiba.mobile.pruebadeingreso.data.rest.endpoint;


import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.data.model.user.User;
import co.com.ceiba.mobile.pruebadeingreso.di.module.RetrofitModule;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
public interface UserEndpoint {

    @GET(RetrofitModule.GET_USERS)
    Single<List<User>> getUsers();

    @GET(RetrofitModule.GET_USERS)
    Single<List<User>> getUser(
            @Query("id") int userId
    );
}
