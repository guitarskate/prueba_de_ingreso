package co.com.ceiba.mobile.pruebadeingreso.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import co.com.ceiba.mobile.pruebadeingreso.BuildConfig;
import co.com.ceiba.mobile.pruebadeingreso.data.rest.endpoint.PostEndpoint;
import co.com.ceiba.mobile.pruebadeingreso.data.rest.endpoint.UserEndpoint;
import co.com.ceiba.mobile.pruebadeingreso.data.rest.interceptor.ErrorInterceptor;
import co.com.ceiba.mobile.pruebadeingreso.data.rest.interceptor.LoggingInterceptor;
import co.com.ceiba.mobile.pruebadeingreso.data.rest.interceptor.RequestInterceptor;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
@Module
public class RetrofitModule {

    private static final String URL_BASE = "https://jsonplaceholder.typicode.com/";
    public static final String GET_USERS = "users";
    public static final String GET_POST_USER = "posts";

    private static final int REQUEST_TIMEOUT = 10;

    @Singleton
    @Provides
    Gson provideGsonCreate(){
        return new GsonBuilder().create();
    }

    @Singleton
    @Provides
    GsonConverterFactory provideGsonConverterFactory(Gson gson){
        return GsonConverterFactory.create(gson);
    }

    @Singleton
    @Provides
    OkHttpClient.Builder provideOkHttpClientBuilder(){
        return new OkHttpClient.Builder().connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS);
    }

    @Singleton
    @Provides
    OkHttpClient provideOkHttpClient(OkHttpClient.Builder okHttpClientBuilder){
        if(BuildConfig.DEBUG) {
            okHttpClientBuilder.addNetworkInterceptor(new LoggingInterceptor());
            okHttpClientBuilder.addInterceptor(new ErrorInterceptor());
        }
        okHttpClientBuilder.addInterceptor(new RequestInterceptor());
        return okHttpClientBuilder.cache(null).build();
    }

    @Singleton
    @Provides
    RxJava2CallAdapterFactory provideRxJavaAdapterFactory(){
        return RxJava2CallAdapterFactory.create();
    }

    @Singleton
    @Provides
    static Retrofit provideRetrofit(OkHttpClient client, GsonConverterFactory gsonConverterFactory, RxJava2CallAdapterFactory rxJavaAdapterFactory) {
        return new Retrofit.Builder()
                .client(client)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaAdapterFactory)
                .baseUrl(URL_BASE)
                .build();
    }

    @Singleton
    @Provides
    static PostEndpoint providePostService(Retrofit retrofit) {
        return retrofit.create(PostEndpoint.class);
    }

    @Singleton
    @Provides
    static UserEndpoint provideUserService(Retrofit retrofit) {
        return retrofit.create(UserEndpoint.class);
    }

}
