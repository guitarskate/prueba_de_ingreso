package co.com.ceiba.mobile.pruebadeingreso.di.component;

import android.app.Application;

import javax.inject.Singleton;

import co.com.ceiba.mobile.pruebadeingreso.base.BaseApplication;
import co.com.ceiba.mobile.pruebadeingreso.di.module.ActivityBindingModule;
import co.com.ceiba.mobile.pruebadeingreso.di.module.ContextModule;
import co.com.ceiba.mobile.pruebadeingreso.di.module.DatabaseModule;
import co.com.ceiba.mobile.pruebadeingreso.di.module.RetrofitModule;
import co.com.ceiba.mobile.pruebadeingreso.di.module.ViewModelModule;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
@Singleton
@Component(
        modules = {
                ContextModule.class,
                RetrofitModule.class,
                AndroidSupportInjectionModule.class,
                ActivityBindingModule.class,
                DatabaseModule.class,
                ViewModelModule.class
        })
public interface ApplicationComponent extends AndroidInjector<DaggerApplication> {

    void inject(BaseApplication application);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        Builder databaseModule(DatabaseModule databaseModule);
        ApplicationComponent build();
    }
}