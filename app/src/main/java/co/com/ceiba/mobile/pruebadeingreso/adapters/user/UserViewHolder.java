package co.com.ceiba.mobile.pruebadeingreso.adapters.user;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.base.BaseActivity;
import co.com.ceiba.mobile.pruebadeingreso.data.model.user.User;
import co.com.ceiba.mobile.pruebadeingreso.ui.activities.post.PostActivity;
import co.com.ceiba.mobile.pruebadeingreso.utils.AnimActivity;
import co.com.ceiba.mobile.pruebadeingreso.utils.Constants;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
class UserViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.contentCard)
    LinearLayout contentCard;

    @BindView(R.id.contentPhone)
    LinearLayout contentPhone;

    @BindView(R.id.contentEmail)
    LinearLayout contentEmail;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.phone)
    TextView phone;

    @BindView(R.id.email)
    TextView email;

    @BindView(R.id.contentBtnViewPost)
    RelativeLayout contentBtnViewPost;

    @BindView(R.id.btn_view_post)
    Button btnViewPost;

    UserViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    void bind(Context context, User user){
        name.setText(Html.fromHtml(user.getName()));
        phone.setText(Html.fromHtml(user.getPhone()));
        email.setText(Html.fromHtml(user.getEmail()));

        btnViewPost.setOnClickListener(v -> {
            Intent intent = new Intent(context, PostActivity.class);
            intent.putExtra(Constants.USER_ID, user.getId());
            AnimActivity.animRightLeft((BaseActivity)context, intent, false);
        });
    }
}