package co.com.ceiba.mobile.pruebadeingreso.di.module;

import android.content.Context;


import androidx.room.Room;

import javax.inject.Singleton;

import co.com.ceiba.mobile.pruebadeingreso.data.dao.PostDao;
import co.com.ceiba.mobile.pruebadeingreso.data.dao.UserDao;
import co.com.ceiba.mobile.pruebadeingreso.data.room.AppDatabase;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Yulian Martinez on 11/01/2020.
 * @author Yulian Martinez
 */
@Module
public class DatabaseModule {
    private final Context context;

    private final String mDBName = "prueba_ingreso.db";

    public DatabaseModule(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    AppDatabase provideDatabase () {
        return Room.databaseBuilder(
                context,
                AppDatabase.class,
                mDBName
        ).fallbackToDestructiveMigration().build();
    }

    @Provides
    String provideDatabaseName() {
        return mDBName;
    }

    @Singleton
    @Provides
    PostDao providePostDao(AppDatabase db) {
        return db.postDao();
    }

    @Singleton
    @Provides
    UserDao provideUserDao(AppDatabase db) {
        return db.userDao();
    }

}
