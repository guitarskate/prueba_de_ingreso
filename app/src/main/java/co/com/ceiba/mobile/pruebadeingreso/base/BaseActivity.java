package co.com.ceiba.mobile.pruebadeingreso.base;

import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import java.util.Objects;

import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
@SuppressWarnings("unused")
public abstract class BaseActivity extends DaggerAppCompatActivity {

    private Toolbar toolbar;

    @LayoutRes
    protected abstract int layoutRes();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutRes());
        ButterKnife.bind(this);
        setToolbar(null);
    }

    /**
     * Set toolbar action bar
     *
     * @param toolbar int id
     */
    public void setToolbar(@Nullable Toolbar toolbar) {
        if (toolbar == null)
            this.toolbar = findViewById(toolbarId());

        if (this.toolbar != null) {
            setSupportActionBar(this.toolbar);
        }
    }

    /**
     * Obtiene el Toolbar de la actividad
     * @return - toolbar
     */
    public Toolbar getToolbar() {
        return toolbar;
    }

    /**
     * Set Support ActionBar
     *
     * @param displayHome si es true indica que regresa un nivel en la UI
     * @param showIconHome muestra el icono en el toolbar
     */
    public void setSupportActionBar(boolean displayHome, boolean showIconHome) {
        if (toolbar != null){
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(displayHome);
            getSupportActionBar().setDisplayShowHomeEnabled(showIconHome);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (toolbar != null)
            finish();
        return false;
    }

    /**
     * Get toolbar id
     *
     * @return R.id.toolbar si en el layout se ha declarado, de lo contrario 0
     */
    protected abstract int toolbarId();

}