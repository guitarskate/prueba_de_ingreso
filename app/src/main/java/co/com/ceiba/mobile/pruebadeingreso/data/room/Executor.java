package co.com.ceiba.mobile.pruebadeingreso.data.room;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Yulian Martinez on 11/01/2020.
 * @author Yulian Martinez
 */
public class Executor {
    public static void IOThread(Runnable t) {
        ExecutorService IO_EXECUTOR = Executors.newSingleThreadExecutor();
        IO_EXECUTOR.execute(t);
    }
}
