package co.com.ceiba.mobile.pruebadeingreso.adapters.user;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.data.model.user.User;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
public class UserAdapter extends RecyclerView.Adapter<UserViewHolder> {

    private List<User> user;
    private Context context;

    public UserAdapter(Context context, List<User> user) {
        this.user = user;
        this.context = context;
    }

    @NonNull
    @Override
    @SuppressLint("InflateParams")
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_list_item, null);

        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserViewHolder userViewHolder, int i) {
        userViewHolder.bind(context, user.get(i));
    }

    public void filterUser(ArrayList<User> filteredUser) {
        user = filteredUser;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return (null != user ? user.size() : 0);
    }
}