package co.com.ceiba.mobile.pruebadeingreso.base;

import android.content.Context;

import androidx.multidex.MultiDex;

import co.com.ceiba.mobile.pruebadeingreso.di.component.ApplicationComponent;
import co.com.ceiba.mobile.pruebadeingreso.di.component.DaggerApplicationComponent;
import co.com.ceiba.mobile.pruebadeingreso.di.module.DatabaseModule;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import io.reactivex.plugins.RxJavaPlugins;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
public class BaseApplication extends DaggerApplication {

    private static BaseApplication instance;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        RxJavaPlugins.setErrorHandler(throwable ->
                System.out.println("ERROR RxJavaPlugin" + throwable.getMessage()));
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent component = DaggerApplicationComponent.builder().application(this)
                .databaseModule(new DatabaseModule(this)).build();
        component.inject(this);

        return component;
    }

    public static Context getAppContext() {
        return instance;
    }

}
