package co.com.ceiba.mobile.pruebadeingreso.ui.activities.main;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.adapters.user.UserAdapter;
import co.com.ceiba.mobile.pruebadeingreso.base.BaseActivity;
import co.com.ceiba.mobile.pruebadeingreso.data.model.post.Post;
import co.com.ceiba.mobile.pruebadeingreso.data.model.user.User;
import co.com.ceiba.mobile.pruebadeingreso.data.room.AppDatabase;
import co.com.ceiba.mobile.pruebadeingreso.data.room.Executor;
import co.com.ceiba.mobile.pruebadeingreso.utils.ViewModelFactory;
import co.com.ceiba.mobile.pruebadeingreso.widgets.ProgressDialogCeiba;
import co.com.ceiba.mobile.pruebadeingreso.widgets.SpacesItemDecoration;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
public class MainActivity extends BaseActivity {

    @Inject
    ViewModelFactory viewModelFactory;
    private MainViewModel viewModel;

    @Inject
    AppDatabase appDatabase;

    @BindView(R.id.editTextSearch)
    EditText edtSearch;

    @BindView(R.id.recyclerViewSearchResults)
    RecyclerView recyclerSearch;

    @BindView(R.id.contentEmpty)
    View contentEmpty;

    private GridLayoutManager searchManager;
    private UserAdapter userAdapter;
    private List<User> users;
    private int current = 0;

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected int toolbarId() {
        return R.id.toolbar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
    }

    /**
     * Inicializa los datos necesarios para la ejecucion de la actividad
     */
    private void initUI(){
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);

        String loadingTitle = getResources().getString(R.string.loading);
        String loadingMessage = getResources().getString(R.string.message_loading_users);
        ProgressDialogCeiba.showDialog(MainActivity.this, loadingTitle, loadingMessage, false);

        users = new ArrayList<>();
        searchManager = new GridLayoutManager(this, 1);
        searchManager.setOrientation(RecyclerView.VERTICAL);

        recyclerSearch.addItemDecoration(new SpacesItemDecoration(30));

        verifyLocalData();                      // Verifica que hayan datos en la base de datos local
        observableViewModel();                  // observa los cambios en el MainViewModel

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    /**
     * Busca usuarios por medio del nombre en el UserAdapter
     * @param name - nombre que se desea bucar
     */
    private void filter(String name) {
        ArrayList<User> filteredUser = new ArrayList<>();

        for (User user : users) {
            if (user.getName().toLowerCase().contains(name.toLowerCase())) {
                filteredUser.add(user);
            }
        }

        showContent(filteredUser.size() > 0);
        userAdapter.filterUser(filteredUser);
    }

    /**
     * Verifica que hayan datos en la base de datos local,
     * de no haber resultados los busca por medio de la API Rest
     */
    private void verifyLocalData(){
        Executor.IOThread(() -> {
            List<User> userList = appDatabase.userDao().getUsers();
            if (userList.size() > 0) {
                addDataRecycler(userList);
            } else {
                viewModel.fetchUsers();
                viewModel.fetchPosts();
            }
        });
    }

    /**
     * Guarda todos los usuarios en la base de datos local
     * @param userList - lista de usuarios
     */
    private void setUsersDatabase(List<User> userList){
        for (User user : userList){
            Executor.IOThread(() -> appDatabase.userDao().insert(user));
        }
    }

    /**
     * Guarda todas las publicaciones de los usuarios en la base de datos local
     * @param postList - lista de publicaciones
     */
    private void setPostsDatabase(List<Post> postList){
        for (Post post : postList){
            Executor.IOThread(() -> appDatabase.postDao().insert(post));
        }
    }

    /**
     * Observa los cambios en el MainViewModel
     */
    private void observableViewModel() {
        viewModel.getUserList().observe(this, userList -> {
            if (userList == null)
                return;

            addDataRecycler(userList);
            setUsersDatabase(userList);
        });

        viewModel.getPostList().observe(this, postList -> {
            if (postList == null)
                return;

            setPostsDatabase(postList);
        });
    }

    /**
     * Agrega los usuarios al RecyclerView de la actividad
     * @param userList - lista de usuarios
     */
    private void addDataRecycler(List<User> userList){
        showContent(userList.size() > 0);

        users.addAll(userList);
        current ++;

        if(current <= 1){
            userAdapter = new UserAdapter(this, users);
            recyclerSearch.setLayoutManager(searchManager);
            recyclerSearch.setAdapter(userAdapter);
            recyclerSearch.getScrollState();
        } else {
            userAdapter.notifyItemInserted(searchManager.getItemCount() - 1);
            userAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Muestra el RecyclerView o el ContentEmpty dependiendo de lo que se desee, y ciera el Progress Dialog
     * @param showContent - true para mostrar el Recycler y ocultar el ContentEmpty
     */
    private void showContent(boolean showContent){
        contentEmpty.setVisibility(showContent ? View.GONE : View.VISIBLE);
        recyclerSearch.setVisibility(showContent ? View.VISIBLE : View.GONE);
        ProgressDialogCeiba.removeDialog();
    }
}
