package co.com.ceiba.mobile.pruebadeingreso.di.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import co.com.ceiba.mobile.pruebadeingreso.di.util.ViewModelKey;
import co.com.ceiba.mobile.pruebadeingreso.ui.activities.main.MainViewModel;
import co.com.ceiba.mobile.pruebadeingreso.ui.activities.post.PostViewModel;
import co.com.ceiba.mobile.pruebadeingreso.utils.ViewModelFactory;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
@Module
public abstract class ViewModelModule {

    // Activities
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel provideMainViewModel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PostViewModel.class)
    abstract ViewModel providePostViewModel(PostViewModel postViewModel);

    // Fragments
//    @Binds
//    @IntoMap
//    @ViewModelKey(MessageFragmentViewModel.class)
//    abstract ViewModel provideMessageFragmentViewModel(MessageFragmentViewModel messageFragmentViewModel);

    @Binds
    abstract ViewModelProvider.Factory provideViewModelFactory(ViewModelFactory factory);

}
