package co.com.ceiba.mobile.pruebadeingreso.data.room;


import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import co.com.ceiba.mobile.pruebadeingreso.data.dao.PostDao;
import co.com.ceiba.mobile.pruebadeingreso.data.dao.UserDao;
import co.com.ceiba.mobile.pruebadeingreso.data.model.post.Post;
import co.com.ceiba.mobile.pruebadeingreso.data.model.user.User;
import co.com.ceiba.mobile.pruebadeingreso.di.util.Converters;

/**
 * Created by Yulian Martinez on 11/01/2020.
 * @author Yulian Martinez
 */
@Database(entities = {Post.class, User.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract PostDao postDao();
    public abstract UserDao userDao();

}
