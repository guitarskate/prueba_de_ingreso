package co.com.ceiba.mobile.pruebadeingreso.ui.activities.post;

import android.util.Log;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.data.model.post.Post;
import co.com.ceiba.mobile.pruebadeingreso.data.model.user.User;
import co.com.ceiba.mobile.pruebadeingreso.data.rest.ApiServiceManager;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
@SuppressWarnings("ConstantConditions")
public class PostViewModel extends ViewModel {

    private ApiServiceManager apiServiceManager;
    private CompositeDisposable disposable;

    private MutableLiveData<List<Post>> postList = new MutableLiveData<>();
    private MutableLiveData<User> user = new MutableLiveData<>();

    @Inject
    PostViewModel(ApiServiceManager apiServiceManager) {
        this.apiServiceManager = apiServiceManager;
        disposable = new CompositeDisposable();
    }

    LiveData<List<Post>> getPostList() {
        return postList;
    }

    LiveData<User> getUser() {
        return user;
    }

    /**
     * Busca un usuario en la API Rest por el id del usuario
     * @param userId - id del usuario
     */
    void fetchUser(int userId) {
        disposable.add(apiServiceManager.getUser(userId).subscribeWith(new DisposableSingleObserver<List<User>>() {
            @Override
            public void onSuccess(List<User> value) {
                user.setValue(value.get(0));
            }

            @Override
            public void onError(Throwable e) {
                user.setValue(null);
                Log.e("Error user", e.getMessage());
            }
        }));
    }

    /**
     * Busca las publicaciones de un usuario en la API Rest por medio del id del usuario
     * @param userId - id del usuario
     */
    void fetchPosts(int userId) {
        disposable.add(apiServiceManager.getPostsByUserId(userId).subscribeWith(new DisposableSingleObserver<List<Post>>() {
            @Override
            public void onSuccess(List<Post> value) {
                postList.setValue(value);
            }

            @Override
            public void onError(Throwable e) {
                postList.setValue(null);
                Log.e("Error posts", e.getMessage());
            }
        }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}