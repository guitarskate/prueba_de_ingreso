package co.com.ceiba.mobile.pruebadeingreso.data.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.base.BaseDao;
import co.com.ceiba.mobile.pruebadeingreso.data.model.post.Post;

/**
 * Created by Yulian Martinez on 11/01/2020.
 * @author Yulian Martinez
 */
@Dao
public interface PostDao extends BaseDao<Post> {

    @Query("SELECT * FROM post")
    List<Post> getAll();

    @Query("SELECT * FROM post WHERE userId LIKE :userId")
    List<Post> getPostsByUserId(int userId);

}
