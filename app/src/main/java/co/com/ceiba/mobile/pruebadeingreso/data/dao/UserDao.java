package co.com.ceiba.mobile.pruebadeingreso.data.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.base.BaseDao;
import co.com.ceiba.mobile.pruebadeingreso.data.model.user.User;

/**
 * Created by Yulian Martinez on 11/01/2020.
 * @author Yulian Martinez
 */
@Dao
public interface UserDao extends BaseDao<User> {

    @Query("SELECT * FROM user")
    List<User> getUsers();

    @Query("SELECT * FROM user WHERE id LIKE :id")
    User getUserById(int id);

}
