package co.com.ceiba.mobile.pruebadeingreso.base;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Update;

/**
 * Created by Yulian Martinez on 11/01/2020.
 * @author Yulian Martinez
 */
@SuppressWarnings({"UnusedReturnValue", "unused"})
@Dao
public interface BaseDao<T>  {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(T t);

    @Update
    void update(T t);

    @Delete
    void delete(T t);
}
