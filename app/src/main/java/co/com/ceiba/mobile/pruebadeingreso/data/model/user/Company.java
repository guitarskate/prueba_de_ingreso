package co.com.ceiba.mobile.pruebadeingreso.data.model.user;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import co.com.ceiba.mobile.pruebadeingreso.di.util.Converters;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
@Entity(tableName = "company")
@TypeConverters(Converters.class)
public class Company {

    @ColumnInfo(name = "company_name")
    @SerializedName("name")
    private String name;

    @ColumnInfo(name = "catchPhrase")
    @SerializedName("catchPhrase")
    private String catchPhrase;

    @ColumnInfo(name = "bs")
    @SerializedName("bs")
    private String bs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCatchPhrase() {
        return catchPhrase;
    }

    public void setCatchPhrase(String catchPhrase) {
        this.catchPhrase = catchPhrase;
    }

    public String getBs() {
        return bs;
    }

    public void setBs(String bs) {
        this.bs = bs;
    }

}