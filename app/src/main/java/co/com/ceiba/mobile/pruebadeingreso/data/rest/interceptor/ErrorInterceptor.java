package co.com.ceiba.mobile.pruebadeingreso.data.rest.interceptor;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;


import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import co.com.ceiba.mobile.pruebadeingreso.base.BaseApplication;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

@SuppressWarnings("ConstantConditions")
public class ErrorInterceptor implements Interceptor {
    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {

        if (!isOnline()){
            backgroundThreadShortToast(BaseApplication.getAppContext(), "Comprueba tu conexión a internet e inténtalo de nuevo");
        }
        Request request = chain.request();
        Response response = chain.proceed(request);

        if ((request.method().equals("POST") || request.method().equals("PATCH") || request.method().equals("PUT"))
                && response.code() >= 400 && response.code() < 600) {
            // Server issues
            String body = getResponse(response);
            if("application/json".equals(response.header("Content-Type"))) {
                backgroundThreadShortToast(BaseApplication.getAppContext(), "Error: " + response.code() + " mira el Logcat");
            }
            else {
                System.out.println("ERROR--------------------------BEGIN");
                System.out.println("URL: " + request.url().toString());
                System.out.println("STATUS: " + response.code());
                System.out.println(body);
                System.out.println("ERROR--------------------------END");
            }
        }

        return response;
    }

    private String getResponse(Response response){
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.body().byteStream()));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private static void backgroundThreadShortToast(final Context context, final String msg)
    {
        if(context != null && msg != null) {
            new Handler(Looper.getMainLooper()).post(() -> Toast.makeText(context, msg, Toast.LENGTH_SHORT).show());
        }
    }

    private boolean isOnline(){
        ConnectivityManager connectivityManager = (ConnectivityManager) BaseApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }
}
