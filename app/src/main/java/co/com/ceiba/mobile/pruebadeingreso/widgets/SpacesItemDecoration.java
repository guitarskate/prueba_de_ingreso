package co.com.ceiba.mobile.pruebadeingreso.widgets;

import android.graphics.Rect;
import android.view.View;

import org.jetbrains.annotations.NotNull;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, @NotNull View view, RecyclerView parent, @NotNull RecyclerView.State state) {
        int SPACE_DEFAULT = 5;
        outRect.left = SPACE_DEFAULT;
        outRect.right = SPACE_DEFAULT;
        outRect.bottom = space;

        // Add top margin only for the first item to avoid double space between items
        if(parent.getChildAdapterPosition(view) == 0) {
            outRect.top = space;
        }
    }
}