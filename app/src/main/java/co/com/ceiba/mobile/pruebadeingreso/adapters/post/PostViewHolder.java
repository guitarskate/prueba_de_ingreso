package co.com.ceiba.mobile.pruebadeingreso.adapters.post;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.data.model.post.Post;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
class PostViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.contentCard)
    LinearLayout contentCard;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.body)
    TextView body;

    PostViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    void bind(Context context, Post post){
        title.setText(Html.fromHtml(post.getTitle()));
        body.setText(Html.fromHtml(post.getBody()));
    }
}