package co.com.ceiba.mobile.pruebadeingreso.di.module;

import co.com.ceiba.mobile.pruebadeingreso.ui.activities.main.MainActivity;
import co.com.ceiba.mobile.pruebadeingreso.ui.activities.post.PostActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
@Module
public abstract class ActivityBindingModule {


    // Activities
    @ContributesAndroidInjector
    abstract MainActivity provideMainActivity();

    @ContributesAndroidInjector
    abstract PostActivity providePostActivity();


    // Fragments
//    @ContributesAndroidInjector
//    abstract MessageFragment provideMessageFragment();

}
