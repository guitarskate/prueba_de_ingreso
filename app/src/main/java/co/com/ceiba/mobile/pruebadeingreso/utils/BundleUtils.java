package co.com.ceiba.mobile.pruebadeingreso.utils;

import androidx.fragment.app.FragmentActivity;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
@SuppressWarnings("unused")
public class BundleUtils {

    public static String String(FragmentActivity context, String key){
        return (context.getIntent().getExtras() != null) ? context.getIntent().getExtras().getString(key) : "";
    }

    public static int Int(FragmentActivity context, String key){
        return (context.getIntent().getExtras() != null) ? context.getIntent().getExtras().getInt(key) : 0;
    }

    public static double Double(FragmentActivity context, String key){
        return (context.getIntent().getExtras() != null) ? context.getIntent().getExtras().getDouble(key) : 0;
    }

    public static boolean Boolean(FragmentActivity context, String key){
        return (context.getIntent().getExtras() != null) && context.getIntent().getExtras().getBoolean(key);
    }
}
