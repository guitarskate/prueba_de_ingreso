package co.com.ceiba.mobile.pruebadeingreso.data.rest.endpoint;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.data.model.post.Post;
import co.com.ceiba.mobile.pruebadeingreso.di.module.RetrofitModule;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
public interface PostEndpoint {

    @GET(RetrofitModule.GET_POST_USER)
    Single<List<Post>> getPosts();

    @GET(RetrofitModule.GET_POST_USER)
    Single<List<Post>> getPostsByUserId(
            @Query("userId") int userId
    );

}
