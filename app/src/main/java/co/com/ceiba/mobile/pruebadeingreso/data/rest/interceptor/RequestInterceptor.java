package co.com.ceiba.mobile.pruebadeingreso.data.rest.interceptor;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
public class RequestInterceptor implements Interceptor {

    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();

        request = request.newBuilder()
                .addHeader("Content-Type", "application/json")
                .build();

//        AccessToken accessToken = AppPreferences.getInstance().getAccessToken();
//        if(accessToken != null){
//            request = request.newBuilder()
//                    .addHeader("Authorization", accessToken.getType() + " " + accessToken.getToken())
//                    .build();
//        }

        return chain.proceed(request);
    }

}
