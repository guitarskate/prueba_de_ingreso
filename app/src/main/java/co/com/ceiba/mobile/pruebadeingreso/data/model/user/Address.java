package co.com.ceiba.mobile.pruebadeingreso.data.model.user;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import co.com.ceiba.mobile.pruebadeingreso.di.util.Converters;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
@Entity(tableName = "address")
@TypeConverters(Converters.class)
public class Address {

    @ColumnInfo(name = "street")
    @SerializedName("street")
    private String street;

    @ColumnInfo(name = "suite")
    @SerializedName("suite")
    private String suite;

    @ColumnInfo(name = "city")
    @SerializedName("city")
    private String city;

    @ColumnInfo(name = "zipcode")
    @SerializedName("zipcode")
    private String zipcode;

    @Embedded
    @SerializedName("geo")
    private Geo geo;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuite() {
        return suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Geo getGeo() {
        return geo;
    }

    public void setGeo(Geo geo) {
        this.geo = geo;
    }

}