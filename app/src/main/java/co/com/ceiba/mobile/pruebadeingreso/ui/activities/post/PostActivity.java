package co.com.ceiba.mobile.pruebadeingreso.ui.activities.post;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.adapters.post.PostAdapter;
import co.com.ceiba.mobile.pruebadeingreso.base.BaseActivity;
import co.com.ceiba.mobile.pruebadeingreso.data.model.post.Post;
import co.com.ceiba.mobile.pruebadeingreso.data.model.user.User;
import co.com.ceiba.mobile.pruebadeingreso.data.room.AppDatabase;
import co.com.ceiba.mobile.pruebadeingreso.data.room.Executor;
import co.com.ceiba.mobile.pruebadeingreso.utils.BundleUtils;
import co.com.ceiba.mobile.pruebadeingreso.utils.Constants;
import co.com.ceiba.mobile.pruebadeingreso.utils.ViewModelFactory;
import co.com.ceiba.mobile.pruebadeingreso.widgets.SpacesItemDecoration;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
public class PostActivity extends BaseActivity {

    @Inject
    ViewModelFactory viewModelFactory;
    private PostViewModel viewModel;

    @Inject
    AppDatabase appDatabase;

    @BindView(R.id.name)
    TextView txtName;

    @BindView(R.id.phone)
    TextView txtPhone;

    @BindView(R.id.email)
    TextView txtEmail;

    @BindView(R.id.recyclerViewPostsResults)
    RecyclerView recyclerPost;

    private GridLayoutManager postManager;
    private PostAdapter postAdapter;
    private List<Post> posts;
    private int current = 0;

    private int userId;

    @Override
    protected int layoutRes() {
        return R.layout.activity_post;
    }

    @Override
    protected int toolbarId() {
        return R.id.toolbar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
    }

    private void initUI(){
        setSupportActionBar(true, true);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PostViewModel.class);
        userId = BundleUtils.Int(this, Constants.USER_ID);

        posts = new ArrayList<>();
        postManager = new GridLayoutManager(PostActivity.this, 1);
        postManager.setOrientation(GridLayoutManager.VERTICAL);

        recyclerPost.addItemDecoration(new SpacesItemDecoration(30));

        verifyLocalData();                      // Verifica que hayan datos en la base de datos local
        observableViewModel();                  // observa los cambios en el MainViewModel
    }

    /**
     * Verifica que hayan datos en la base de datos local,
     * de no haber resultados los busca por medio de la API Rest
     */
    private void verifyLocalData(){
        Executor.IOThread(() -> {
            User user = appDatabase.userDao().getUserById(userId);
            if(user != null){
                setDataUI(user);
            } else {
                viewModel.fetchUser(userId);
            }

            List<Post> postList = appDatabase.postDao().getPostsByUserId(userId);
            if (postList.size() > 0) {
                addDataRecycler(postList);
            } else {
                viewModel.fetchPosts(userId);
            }
        });
    }

    /**
     * Guarda todas las publicaciones de los usuarios en la base de datos local
     * @param postList - lista de publicaciones
     */
    private void setPostDatabase(List<Post> postList){
        for (Post post : postList){
            System.out.println("GUARDA _POST : " + post.getId());
            Executor.IOThread(() -> appDatabase.postDao().insert(post));
        }

        Executor.IOThread(() -> {
            for (Post post : appDatabase.postDao().getAll()){
                System.out.println("SELECT: " + post.getTitle());
            }
        });
    }

    /**
     * Guarda un usuario en la base de datos local
     * @param user - usuario que se desea guardar
     */
    private void setUsetDatabase(User user){
        Executor.IOThread(() -> appDatabase.userDao().insert(user));
    }

    /**
     * Muestra la informacion del usuario
     * @param user - usuario
     */
    private void setDataUI(User user){
        txtName.setText(user.getName());
        txtPhone.setText(user.getPhone());
        txtEmail.setText(user.getEmail());
    }

    /**
     * Observa los cambios en el MainViewModel
     */
    private void observableViewModel() {
        viewModel.getUser().observe(this, user -> {
            if (user == null)
                return;

            setDataUI(user);
            setUsetDatabase(user);
        });

        viewModel.getPostList().observe(this, postList -> {
            if (postList == null)
                return;

            addDataRecycler(postList);
            setPostDatabase(posts);
        });
    }

    /**
     * Agrega las publicaciones al RecyclerView de la actividad
     * @param postList - lista de publicaciones
     */
    private void addDataRecycler(List<Post> postList){
        showContent(postList.size() > 0);

        posts.addAll(postList);
        current ++;

        if(current <= 1){
            postAdapter = new PostAdapter(this, posts);
            recyclerPost.setLayoutManager(postManager);
            recyclerPost.setAdapter(postAdapter);
            recyclerPost.getScrollState();
        } else {
            postAdapter.notifyItemInserted(postManager.getItemCount() - 1);
            postAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Muestra el RecyclerView y ciera el Progress Dialog
     * @param showContent - true para mostrar el Recycler
     */
    private void showContent(boolean showContent){
        recyclerPost.setVisibility(showContent ? View.VISIBLE : View.GONE);
    }
}
