package co.com.ceiba.mobile.pruebadeingreso.ui.activities.main;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import co.com.ceiba.mobile.pruebadeingreso.data.model.post.Post;
import co.com.ceiba.mobile.pruebadeingreso.data.model.user.User;
import co.com.ceiba.mobile.pruebadeingreso.data.rest.ApiServiceManager;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
@SuppressWarnings("ConstantConditions")
public class MainViewModel extends ViewModel {

    private ApiServiceManager apiServiceManager;
    private CompositeDisposable disposable;

    private MutableLiveData<List<User>> userList = new MutableLiveData<>();
    private MutableLiveData<List<Post>> postList = new MutableLiveData<>();

    @Inject
    MainViewModel(ApiServiceManager apiServiceManager) {
        this.apiServiceManager = apiServiceManager;
        disposable = new CompositeDisposable();
    }

    LiveData<List<User>> getUserList() {
        return userList;
    }

    LiveData<List<Post>> getPostList() {
        return postList;
    }

    /**
     * Busca los usuarios en la API Rest y agrega
     */
    void fetchUsers() {
        disposable.add(apiServiceManager.getUsers().subscribeWith(new DisposableSingleObserver<List<User>>() {
            @Override
            public void onSuccess(List<User> value) {
                userList.setValue(value);
            }

            @Override
            public void onError(Throwable e) {
                userList.setValue(null);
                Log.e("Error Comment", e.getMessage());
            }
        }));
    }

    /**
     * Busca las publicaciones en la API Rest
     */
    void fetchPosts() {
        disposable.add(apiServiceManager.getPosts().subscribeWith(new DisposableSingleObserver<List<Post>>() {
            @Override
            public void onSuccess(List<Post> value) {
                postList.setValue(value);
            }

            @Override
            public void onError(Throwable e) {
                postList.setValue(null);
                Log.e("Error posts", e.getMessage());
            }
        }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}