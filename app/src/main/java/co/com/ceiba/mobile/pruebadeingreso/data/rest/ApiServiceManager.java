package co.com.ceiba.mobile.pruebadeingreso.data.rest;

import java.util.List;

import javax.inject.Inject;

import co.com.ceiba.mobile.pruebadeingreso.data.model.post.Post;
import co.com.ceiba.mobile.pruebadeingreso.data.model.user.User;
import co.com.ceiba.mobile.pruebadeingreso.data.rest.endpoint.PostEndpoint;
import co.com.ceiba.mobile.pruebadeingreso.data.rest.endpoint.UserEndpoint;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Yulian Martinez on 10/01/2020.
 * @author Yulian Martinez
 */
public class ApiServiceManager {

    private final PostEndpoint postEndpoint;
    private final UserEndpoint userEndpoint;

    @Inject
    ApiServiceManager(PostEndpoint postEndpoint, UserEndpoint userEndpoint) {
        this.postEndpoint = postEndpoint;
        this.userEndpoint = userEndpoint;
    }

    public Single<List<User>> getUsers() {
        return userEndpoint.getUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<User>> getUser(int userId) {
        return userEndpoint.getUser(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<Post>> getPosts(){
        return postEndpoint.getPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<Post>> getPostsByUserId(int userId){
        return postEndpoint.getPostsByUserId(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
